---
layout: page
redirect_from:
    - /wiki/著作権について_(素材等の提供者の皆様へ)
---

# 著作権について　（素材等の提供者の皆様へ）

作品の提供をご検討いただきありがとうございます。


もしまだお読みでなければ、「[著作権について](./)」を先にお読み下さい。

## 超重要事項 
* **あなたご自身が著作権を保有していない作品を許諾なしに投稿しないで下さい！**
    * 作品をご提供いただく場合は、あなたご自身が著作権を保有していることをお約束下さい。
* **OPAP-JPとは無関係のフリーな素材作品（いわゆる"フリー素材"）をご使用になる場合は、投稿前に一度ご相談下さい。**
    * OPAP-JPが採用している利用許諾の都合上、利用できない素材作品があるためです。
* **あなたの投稿した作品を、他人が遠慮なく編集したり自由に配布したりすることを望まない場合は、投稿しないでください。**
    * クリエイティブ・コモンズ・ライセンスの規定を良くご理解の上、ご投稿ください。
* **作品は、ご自身の責任においてご投稿下さい。**
    * 上記の点をお守り頂けなかった為に発生したトラブル、訴訟、紛争等は、ご自身でご解決いただくことになります。OPAP-JP及びあなたを除くOPAP-JPのメンバーは一切の責任を負いかねます。

OPAP-JPが提供する一切のパブリックスペースに作品を投稿された時点で、上記の点に同意いただいたものとします。


## 作品を投稿する手順 

### ■STEP1. 作品に適用されるライセンス（利用許諾）を確認する 

OPAP-JPでは、標準のライセンスとして、以下のライセンスを採用しております。（ライセンスとは作品の利用許諾のこと）

* **クリエイティブ・コモンズ 表示 2.1 日本   (Creative Commons Attribution 2.1 Japan)**（以下「**CC-BY 2.1 JP**」と呼ぶ。）[ライセンス詳細](https://creativecommons.org/licenses/by/2.1/jp/)
* **クリエイティブ・コモンズ 表示 4.0 国際   (Creative Commons Attribution 4.0 International)**（以下「**CC-BY 4.0**」と呼ぶ。） [ライセンス詳細](https://creativecommons.org/licenses/by/4.0/deed.ja)


作品によっては、上記と異なるライセンスが適用される場合もあります。それぞれの作品に適用されるライセンスをご確認ください。


なお、「クリエイティブ・コモンズ 表示」には、以下のような特徴があります。


* **クレジット表示をすることを条件**に、世界中の誰でも著作権者に断りなく**自由に利用できる**
* **営利利用**ができる

第三者にフリー素材として利用されることにご留意ください。

　
### ■STEP2. 貢献者ライセンス同意書（CLA）への同意 

プロジェクトに参加される全員に貢献者ライセンス同意書の提出をお願いしております。

詳しくは、[こうしす！資料集 - CLAへの同意](https://kosys.gitlab.io/kosys-docs/manuals/cla.html)をご覧ください。


### ■STEP3. 制作に参加し作品を投稿する 



