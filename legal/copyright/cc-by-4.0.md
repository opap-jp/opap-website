---
layout: page
redirect_from:
    - /cc/by/4.0/
redirect_to:
    - https://creativecommons.org/licenses/by/4.0/
---

<https://creativecommons.org/licenses/by/4.0/>に移動します。
