---
layout: page
redirect_from:
    - /wiki/著作権について
---
# 著作権について

OPAP-JPが発表する作品は、以下の利用許諾のもとに利用が許諾されています。
作品の利用者は以下のいずれかの利用許諾を選択できます。

* クリエイティブ・コモンズ 表示 2.1 日本 (Creative Commons Attribution 2.1 Japan) （以下「CC-BY 2.1 JP」と呼ぶ。）
* クリエイティブ・コモンズ 表示 4.0 国際 (Creative Commons Attribution 4.0 International) （以下「CC-BY 4.0」と呼ぶ。）

ただし、例外事項に該当する場合は、その限りではありません。
また、これらの作品には異なる利用許諾のもとに提供されている作品を素材として含む場合もあります。詳細は各作品の著作権情報をご参照下さい。

## クリエイティブ・コモンズ 表示
![CC-BY](/img/cc-by.png)

これは利用許諾条項の重要な事項を一般の方に分かりやすいよう要約したものです。この説明に法的な意味はありません。必ず、以下の利用許諾条項をご覧下さい。

* [CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/legalcode)
* [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode.ja)

<table class="wikitable" style="table-layout: fixed; min-width: 760px;">
<tbody><tr>
<th style="background-color: #FFFF60; width: 33.33%;">守る必要があること
</th>
<th style="background-color: #60FF60; width: 33.33%;">できること
</th>
<th style="background-color: #FF6060; width: 33.33%;">できないこと
</th></tr>
<tr>
<td style="background-color: #FFFFC0; vertical-align: top;">
<dl><dt>表示</dt>
<dd>あなたは適切なクレジットを表示しなければなりません。（→[適切なクレジットとは](./for-users)）</dd></dl>
<p><span style="font-size: 50%; color: #666666;">※ただし、許諾者から別途許可を得た場合は、必要ありません。<br>
※許諾者からクレジットからの除去を要求された場合、それに従わなければなりません。</span>
</p>
</td>
<td style="background-color: #C0FFC0; vertical-align: top;">
<dl><dt>共有</dt>
<dd>複製、頒布　<span style="font-size: 50%; color: #666666;">（媒体・形式を問わず）</span></dd>
<dt>二次的著作物の作成</dt>
<dd>リミックス、変形、別作品の作成</dd>
<dt>営利目的の利用</dt>
<dd>目的・営利非営利を問わず利用が可能</dd>
<dd></dd></dl>
<p><span style="font-size: 50%; color: #666666;"> ※あなたが利用許諾条項に従う限り、許諾者は上記の自由を剥奪することはできません。</span>
</p>
</td>
<td style="background-color: #FFC0C0; vertical-align: top;">
<ul><li>他の作品利用者が利用許諾条件を遵守することを妨げる方法で作品を共有、リミックス等を行うこと。
<dl><dd>無許可で、クレジット表示義務が免除されていると謳って再頒布する等</dd></dl></li>
<li>名誉又は声望を害する方法で改作、変形もしくは翻案すること（CC-BY 2.1 JPのみ）</li>
<li>その他、利用許諾に違反する形での利用</li></ul>
</td></tr></tbody></table>


## 例外事項

以下のいずれかに該当するものに関しては上記の規定を適用しません。利用許諾については個別にお問い合わせください。

* OPAP-JPの非公開スペース（例：閲覧に認証が必要なページなど）に掲載されている場合
* 異なる利用許諾が明示されている場合
* 二次利用の禁止が明示されている場合
* http://jira.opap.jp/ への投稿に使用されているアバター画像
* チャットにおける発言
* ニコニコ生放送
* ソーシャルメディア公式アカウントを通じた投稿及びそれに対する第三者のコメントなどの投稿

## ケース別のご説明

* [作品を提供したい](./for-contributors)
* [作品を利用したい](./for-users)