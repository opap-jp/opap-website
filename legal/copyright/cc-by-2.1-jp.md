---
layout: page
redirect_from:
    - /cc/by/2.1/jp
redirect_to:
    - https://creativecommons.org/licenses/by/2.1/jp 
---

<https://creativecommons.org/licenses/by/2.1/jp>に移動します。
