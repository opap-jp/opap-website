---
title: 会員向けページ
layout: page
---

# 会員向けページ

## コミュニケーション
* チャット <https://chat.opap.jp>
* 進捗管理システムJIRA <https://opap.atlassian.net/>
* Git <https://gitlab.com/kosys/>

## 資料
* こうしす！資料集 <https://kosys.gitlab.io/kosys-docs/> （準備中）


