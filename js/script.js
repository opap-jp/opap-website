$(function () {
const getTop = '/index.html';

$.ajax({
type: 'GET',
url: getTop,
dataType: 'html',
timeout: 30000
})
.then(function (data) {
const getHeader = $(data).find('#headerFrame').contents();
const getFooter = $(data).find('footer').contents();

$('#headerFrame').html(getHeader),
$('footer').html(getFooter);

},
function () {

$('#headerFrame').html('読み込みに失敗しました'),
$('footer').html('読み込みに失敗しました');

});

});