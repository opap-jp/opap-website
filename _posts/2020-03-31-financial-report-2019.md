---
layout: post
title: '2019年12月期の会計報告を掲載しました'
author: OPAP-JP
date: 2020-03-31 00:00:00 +0900
category: news
---

皆様の温かいご支援に心より御礼申し上げます。

以下のページに、2019年12月期の会計報告を掲載いたしましたのでお知らせ致します。



<a href="{{ site.baseurl }}/activity/financial-reports/2019">第8期（2019年12月期）会計報告</a>