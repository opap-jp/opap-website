---
title: コウシス！LITE 第1話 クレジット
layout: page
redirect_from:
    - /wiki/Redirect/kosys_lite_ep01_authors
    - /wiki/redirect/kosys_lite_ep01_authors
    - /wiki/こうしす！/LITE第1話クレジット
---

# コウシス！LITE 第1話 クレジット


## ライセンス 

*  クリエイティブ・コモンズ 表示 4.0  https://creativecommons.org/licenses/by/4.0/deed.ja
*  クリエイティブ・コモンズ 表示 2.1 日本  https://creativecommons.org/licenses/by/2.1/jp

## 貢献者の一覧
利用の際には以下に示す全員についてクレジットの表示が必要です。
<pre>
Butameron
Music is VFR (External material(s) / CC-BY 4.0 / Modified)
えつむ
すら
なめたけ
はるまきごはん
もぐもぐ
るみあ
上妻九
夏野未来
小豆戒斗
桜瀬尋
玉虫型偵察器
絢嶺るり
芋
那々海ゆあ
野口あきら
鈴郷（さとー）
</pre>

## エンディングクレジット
### 声の出演

* 祝園 アカネ:えつむ
* 山家 宏佳:小豆 戒斗
* 垂水 結菜:那々海ゆあ
* 英賀保 芽依: 桜瀬尋
* 少佐:芋
* 汎用型移動式パキラ:鈴郷（さとー）

### キャラクターデザイン
* 夏野未来
* るみあ

### キャラクター原案
* 絢嶺るり
* Butameron

### 原画・動画・仕上
* Butameron（こうしす！#1より）
* 上妻九
* すら
* 夏野未来
* なめたけ
* はるまきごはん
* るみあ（こうしす！#2より）
* もぐもぐ（キャラクターデザインより）
* 夏野未来
* るみあ

### 背景美術
* Butameron（こうしす！#1より）
* 上妻九
* 夏野未来
* なめたけ
* はるまきごはん

### 貼込み素材
* Butameron
* 上妻九

### 作画改変
* Butameron

### レイアウト原案
* 玉虫型偵察器

### 音響監督
* 夏野未来

### サウンドミキサー
* 野口あきら （v1.0.0以降）
* 夏野未来 （v0.9.x）

### 音響効果・音楽
* 夏野未来（こうしす！#1より）
* 焼田儚（外部素材）
* Music is VFR (CC-BY 4.0)

### 撮影
* Butameron（こうしす！#1より）
* 玉虫型偵察器

### 脚本
* Butameron

### コンテ
* ？？？

### 演出
* Butameron

### オープニングテーマ
Try!（発車メロディver）  
編曲:夏野未来  
原曲:Melodic Taste http://taste-f.sakura.ne.jp/Melodic/

### スペシャルサンクス
* サーバー提供 :ConoHa byGMO https://www.conoha.jp/
* 広告掲載: 虚構新聞社 http://kyoko-np.net/
* 制作費提供: OPAP-JP 賛助会員

そして、この作品をご覧頂いた皆様方

本作品の一部の作画にwaifu2xを使用しています。

### 商標について
*  「Twitter」は、Twitter, Inc.の商標または登録商標です。
*  「ニコ動」および「nicovideo」は、株式会社ドワンゴの商標または登録商標です。

本作品には説明の都合上、実在の商品名、サービス名、会社名、または、類似する名称に言及する場面が含まれておりますが、作中における設定は架空のものであり、批判や商標権の侵害を意図したものではありません。

### 掲載クレジットについて
この作品には各素材作品を改変した上で使用しております。
クレジットへの掲載は、必ずしも本作品またはその制作者への後援、支持、賛同などを意味するものではありません。

### 監督
* Butameron

### 企画・制作
* Open Process Animation Project Japan
