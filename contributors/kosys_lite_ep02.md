---
title: コウシス！LITE 第2話 クレジット
layout: page
---

# コウシス！LITE 第2話 クレジット


## 著作権について(Copyright Notice)

この作品を利用するためには以下の内容を適切に表示する必要があります。<br />
To use this work, you must give appropriate credit.<br />

<div style="background-color:#FFEEDD; padding: 10px 10px; border-radius: 10px 10px 10px 10px; border: solid 1px #AA6600;"> 

<span style="font-size: 1.5em; font-weight: bold;">OPAP-JP contributors. <br />
https://opap.jp/contributors</span>

</div>


### ライセンス (License)

以下の著作物利用許諾により利用が許諾されています:
(Licensed under the following license(s):)

* CC-BY 2.1 JP
<http://creativecommons.org/licenses/by/2.1/jp/>

* CC-BY 4.0
<http://creativecommons.org/licenses/by/4.0/>

![CC-BY]({{ site.baseurl }}/img/cc-by.png)

#### 注記
* 本作品には異なるライセンスのフォントを使用して作成された画像が含まれています。これは、各ライセンスの条項と、日本におけるタイプフェイスの著作権に関する判例を総合的に考慮した結果、フォントとして機能しない状態で単に画像として映像に含める場合には、CC-BYライセンスと矛盾しないと判断したものです。ただし、この判断は法律の専門家によるものではないことにご留意ください。

## 注意事項

* この作品はフィクションです。ストーリー、名称、人物、事例はすべて架空のものです。実在の人物、場所、建物、製品とは一切関係ありません。<br />This is a work of fiction. The story, all names, characters and incidents, portrayed in this work are fictitious. No identification with actual persons, places, buildings and products is intended or should be inferred.
* 本作品には説明の都合上、実在の製品名、サービス名、会社名、または、類似する名称に言及する場面が含まれておりますが、作中における設定は架空のものであり、批判または商標権その他の権利の侵害を意図したものではありません。実在の製品名、サービス名、会社名は一般に各社の登録商標または商標です。


## クレジット

### 声の出演

* 祝園 アカネ: えつむ
* 山家 宏佳: 小豆戒斗
* 垂水 結菜: 那々海ゆあ
* 英賀保 芽依: 桜瀬尋
* 中舟生 良文（CIO）:  岡松　丈 

### キャラクターデザイン
* 夏野未来
* るみあ

### キャラクター原案
* 絢嶺るり
* 井二かける

### 原画/動画/仕上
* るみあ
* リンゲリエ

### 背景3D モデル
* 小澤佑太
* くまのぐり
* sacorama
* 井二かける

### 背景
* 廣田智亮
* 京姫鉄道合同会社

### 制服設定
* 麦の人

### サウンドミキサー
* 野口あきら

### 音響効果
* がんくま

### 音楽
* 夏野未来

### 撮影
* 玉虫型偵察器

### 脚本・コンテ・演出
* 井二かける

### 音響制作進行・エンドカード
* 京姫鉄道合同会社

### エンドカード3Dモデル
* 佐久間蒼乃

### オープニングテーマ
Try!（発車メロディver）  
編曲:夏野未来  
原曲:Melodic Taste http://taste-f.sakura.ne.jp/Melodic/

### スペシャルサンクス

#### こうしす！ EE クラウドファンディング支援者の皆様
* https://www.kyoki-railway.co.jp/special/cf/kosys-ee-2019.html#%E8%AC%9D%E8%BE%9E

#### ぽん酢しょうゆ協力
* こむらさき醸造有限会社

### 注意事項
#### 商標について
本作品には説明の都合上、実在の商品名、サービス名、会社名、
または、類似する名称に言及する場面が含まれておりますが、
作中における設定は架空のものであり、批判や商標権の侵害
を意図したものではありません。

#### 掲載クレジットについて
本作品には各素材作品を改変した上で使用しております。クレジットへの掲載は、必ずしも本作品またはその制作者への後援、支持、賛同などを意味するものではありません。

#### 本作品について
本作品は、京姫鉄道合同会社が制作する商用シリーズである「こうしす！EE」シリーズとの無償コラボ作品として制作されました。本作品は、@ITへの連載漫画及び書籍「こうしす！社内SE祝園アカネの情報セキュリティ事件簿」の内容に基づいています。