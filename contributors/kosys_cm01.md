---
title: こうしす！CM01 クレジット
layout: page
redirect_from:
    - /wiki/Redirect/kosys_cm01_authors
    - /wiki/redirect/kosys_cm01_authors
    - /wiki/こうしす！/CM01クレジット
---

# こうしす！CM01 クレジット



## 著作権について(Copyright Notice)

この作品を利用するためには以下の内容を適切に表示する必要があります。<br />
To use this work, you must give appropriate credit.<br />

<div style="background-color:#FFEEDD; padding: 10px 10px; border-radius: 10px 10px 10px 10px; border: solid 1px #AA6600;" markdown="1"> 

<span style="font-size: 1.5em; font-weight: bold;">Copyright &copy; 2016 OPAP-JP contributors.</span>

### クレジット表示 (Attribution of Credit)

<pre>
如月ほのか
Butameron
Lingerie
Music is VFR (CC-BY 4.0)
Stars/Dover
えつむ
すら
なめたけ
もぐもぐ
もんじゅ
るみあ
上妻九
夏野未来
安坂　悠
山春黄
旭洋
武本譲治
麦の人
</pre>


* この作品は、上記の皆様方による作品・素材・資料類を翻案して制作されました。<br />(This work is a adaptation of materials created by authors listed in the above. )
* 上記の一覧は本作品への支持や賛同を示すものではありません。<br />(The above list does not indicate any endorsements for this work.)

### ライセンス (License)

以下の著作物利用許諾により利用が許諾されています:
(Licensed under the following license(s):)

* CC-BY 2.1 JP
<http://creativecommons.org/licenses/by/2.1/jp/>

* CC-BY 4.0
<http://creativecommons.org/licenses/by/4.0/>


</div>

#### 注記
* 本作品には異なるライセンスのフォントを使用して作成された画像が含まれています。これは、各ライセンスの条項と、日本におけるタイプフェイスの著作権に関する判例を総合的に考慮した結果、フォントとして機能しない状態で単に画像として映像に含める場合には、CC-BYライセンスと矛盾しないと判断したものです。ただし、この判断は法律の専門家によるものではないことにご留意ください。


## 注意事項

* この作品はフィクションです。ストーリー、名称、人物、事例はすべて架空のものです。実在の人物、場所、建物、製品とは一切関係ありません。<br />This is a work of fiction. The story, all names, characters and incidents, portrayed in this work are fictitious. No identification with actual persons, places, buildings and products is intended or should be inferred.


## 監督・コンテ・演出・編集
* 如月ほのか

## プロデューサー・制作進行
* 井二かける

## 作画・レイアウト

* Butameron
* Lingerie
* Stars/Dover
* すら
* なめたけ
* もぐもぐ
* もんじゅ
* るみあ
* 上妻九
* 夏野未来
* 如月ほのか
* 安坂　悠
* 山春黄
* 旭洋
* 武本譲治


## 原作
*  「こうしす！」  (OPAP-JP contributors.)
* 原作コンテ
    * 玉虫型偵察器
    * Butameron

## ナレーション

* 祝園アカネ :えつむ

## 音響
* 音響
    * 夏野未来
    * 如月ほのか

* 効果音
    * Music is VFR (CC-BY 4.0)

## テーマ曲
「Try!」 <br />
歌: aosaki <br />
作詞: Butameron <br />
作曲/編曲: Melodic Taste  (http://taste-f.sakura.ne.jp/Melodic/)<br />
MIX: 夏野未来

## キャラクターデザイン
* るみあ
* 夏野未来
* なめたけ
* 武本譲治

## 制服設定
* 麦の人
