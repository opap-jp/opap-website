---
title: OPAP-JP 作品クレジット (OPAP-JP contributors)
layout: page
redirect_from:
    - /wiki/こうしす！/クレジット
---

# OPAP-JP 作品クレジット (OPAP-JP contributors)

OPAP-JPが公表する作品の貢献者一覧です。  
OPAP-JPの作品は、以下の各クレジットに表示された皆様の素材・資料に基づき、複製、翻案、改変等を行い制作されています。


## こうしす！シリーズ
* [共通](./kosys_common)
* [第1話](./kosys_ep01)
* [第2話](./kosys_ep02)
* [第3話](./kosys_ep03)


## コウシス！LITEシリーズ
* [LITE第1話](./kosys_lite_ep01)
* [LITE第2話](./kosys_lite_ep02)


## こうしす！EEシリーズ

こうしす！EEシリーズは、OPAP-JPの「こうしす！」の素材・資料を元に京姫鉄道合同会社が制作している商業向けシリーズです。
当プロジェクトの管轄下にはありません。当該シリーズの貢献者一覧は、京姫鉄道合同会社のページをご覧ください。

* [京姫鉄道合同会社 貢献者一覧](https://www.kyoki-railway.co.jp/contributors)

## その他
* [CM01](./kosys_cm01)
* [醤油](./kosys_soysauce)


## 注記

* このページに記載されているクレジットは、必ずしも各人の作品への賛同･支持などを表明するものではありません。あくまでも、ライセンス上求められているクレジットの表示として掲載しております。
* このページへのリンク、URLまたは「OPAP-JP contributors」の名義が第三者の作品等のクレジットに掲載されている場合、当該作品等はOPAP-JPの作品を複製、翻案、改変等されて制作された作品です。当該クレジットに関して、OPAP-JPが賛同・支持を表明するものではありません。


