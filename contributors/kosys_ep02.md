---
title: こうしす！第2話 クレジット
layout: page
redirect_from:
    - /wiki/redirect/kosys_ep02_authors
    - /wiki/Redirect/kosys_ep02_authors
    - /wiki/こうしす！/第2話クレジット
---

# こうしす！第2話 クレジット


## 著作権について(Copyright Notice)

この作品を利用するためには以下の内容を適切に表示する必要があります。<br />
To use this work, you must give appropriate credit.<br />

<div style="background-color:#FFEEDD; padding: 10px 10px; border-radius: 10px 10px 10px 10px; border: solid 1px #AA6600;" markdown="1"> 
<span style="font-size: 1.5em; font-weight: bold;">Copyright &copy; 2016 OPAP-JP contributors.</span>

### クレジット表示 (Attribution of Credit)

<pre>
Butameron
旭洋
玉虫型偵察器
なめたけ
夏野未来
るみあ
もぐもぐ
奈良有限動画
uvte
R子
Lingerie
たぃと
OPAP-JP
山春黄
しぎ
Stars/Dover
もんじゅ
武本譲治
安坂　悠
五十野タラ
アイラク
藻
上妻九
ゆき
クニキチ
tashi
チャンネル君
絢嶺るり
麦の人
ふぶき
sacorama
かわしろ21
AyneInchNails
yoshina
櫛
すら
川﨑哲也 (from http://sakikawa.michikusa.jp/index.html「昭和30年代の車輌達（堀越和正氏の世界）」)
はるまきごはん
Eric Bradford
しみたか (早稲田大学グリークラブ)
おびの (早稲田大学グリークラブ)
きりさん (早稲田大学グリークラブ)
ベイちゃん (早稲田大学グリークラブ)
8月もポーチ (早稲田大学グリークラブ)
向日 葵@ピカテュウのお面 (早稲田大学グリークラブ)
@smithreen (早稲田大学グリークラブ)
GTタカオ (早稲田大学グリークラブ)
おっくん (早稲田大学グリークラブ)
AK2 (早稲田大学グリークラブ)
SEKAINORYO (早稲田大学グリークラブ)
クリプチッド (早稲田大学グリークラブ)
なおゆき (早稲田大学グリークラブ)
こっしー (早稲田大学グリークラブ)
aosaki
MelodicTaste (http://taste-f.sakura.ne.jp/Melodic/)
雲丹
WIP (http://wip-music.jp/)
ひょん
Music is VFR (CC-BY 4.0)
だいすけ (from https://youtu.be/QYmWmrI4ezg)
えつむ
小豆戒斗
那々海ゆあ
芋
桜瀬尋
岡松　丈
杜川　彦
瀬戸　陽子
NAOKA
武川楓
べ～
浅沼諒空
T-182
矢塚翔
がんくま
野口あきら
</pre>


* この作品は、上記の皆様方による作品・素材・資料類を翻案して制作されました。<br />(This work is a adaptation of materials created by authors listed in the above. )
* 上記の一覧は本作品への支持や賛同を示すものではありません。<br />(The above list does not indicate any endorsements for this work.)

### ライセンス (License)

以下の著作物利用許諾により利用が許諾されています:
(Licensed under the following license(s):)

*  CC-BY 2.1 JP
:http://creativecommons.org/licenses/by/2.1/jp/
:
*  CC-BY 4.0
:http://creativecommons.org/licenses/by/4.0/

![CC-BY]({{ site.baseurl }}/img/cc-by.png)

</div>

#### 注記
* 本作品には異なるライセンスのフォントを使用して作成された画像が含まれています。これは、各ライセンスの条項と、日本におけるタイプフェイスの著作権に関する判例を総合的に考慮した結果、フォントとして機能しない状態で単に画像として映像に含める場合には、CC-BYライセンスと矛盾しないと判断したものです。ただし、この判断は法律の専門家によるものではないことにご留意ください。
* オープニングクレジット・エンディングクレジットは、作品の利用の際に表示しなければならない正式なクレジット表示ではありません。作品の利用の際は上記の表示をお使いください。
* 上記の一覧は順不同です。ファイルサーバーに最初に投稿した日をもとに掲載順を決定しています。投稿日が不明な場合は、一覧の最後に手入力により掲載されています。


## 注意事項

* この作品はフィクションです。ストーリー、名称、人物、事例はすべて架空のものです。実在の人物、場所、建物、製品とは一切関係ありません。<br />This is a work of fiction. The story, all names, characters and incidents, portrayed in this work are fictitious. No identification with actual persons, places, buildings and products is intended or should be inferred.
* 本作品には説明の都合上、実在の製品名、サービス名、会社名、または、類似する名称に言及する場面が含まれておりますが、作中における設定は架空のものであり、批判または商標権その他の権利の侵害を意図したものではありません。実在の製品名、サービス名、会社名は一般に各社の登録商標または商標です。


## 脚本
*  Butameron

## コンテ
*  Butameron
*  玉虫型偵察器
*  五十野タラ

## 演出
*  Butameron
*  玉虫型偵察器

## 声の出演

* 祝園アカネ: えつむ
* 山家宏佳: 小豆戒斗
* 垂水結菜: 那々海ゆあ
* 少佐: 芋
* 英賀保芽依: 桜瀬尋
* 中舟生CIO:  岡松　丈 
* 夢前さくら: 杜川　彦
* 神谷心陽: 瀬戸　陽子
* 網干茉莉: NAOKA
* クロエ・ジェフリーズ: 武川楓
* 葛城（総務部長）:	べ～
* 月田万作（助役）: 浅沼諒空
* 下山浩二: T-182
* 車折秀一（社長）: T-182
* イケル・ヤン: 矢塚翔
* 篠山砂沙美: 桜瀬尋
* 飾磨: 矢塚翔
* 亀山: T-182
* 砥堀: 矢塚翔
* 仁豊野: べ～
* 香呂: T-182
* 甘地: べ～
* 鶴居: 矢塚翔
* 新野: 矢塚翔
* 寺前: べ～
* エンプロイぃ社員: べ～
* 生野加代: 武川楓
* 光亜ど　ヒカル: 矢塚翔
* 向山車掌: 桜瀬尋
* 向河原: T-182

## キャラクターデザイン
* るみあ
* 夏野未来
* なめたけ
* 武本譲治

## キャラクター原案
* Butameron
* 絢嶺るり

## 制服デザイン
* 麦の人
* なめたけ

## 3Dモデル作成
* sacorama


## キャラクター作画
*  Butameron
*  Eric Bradford
*  Lingerie
*  R子
*  Stars/Dover
*  Tashi
*  uvte
*  しぎ
*  たぃと
*  なめたけ
*  もぐもぐ
*  もんじゅ
*  ゆき
*  るみあ
*  アイラク
*  五十野タラ
*  夏野未来
*  奈良有限動画
*  安坂　悠
*  山春黄
*  旭洋
*  武本譲治

## 背景・貼込素材
*  Butameron
*  Lingerie
*  Stars/Dover
*  uvte
*  しぎ
*  たぃと
*  なめたけ
*  はるまきごはん
*  もぐもぐ
*  もんじゅ
*  アイラク
*  上妻九
*  五十野タラ
*  夏野未来
*  安坂　悠
*  武本譲治
*  玉虫型偵察器
*  藻

## レイアウト
*  山春黄
*  玉虫型偵察器

## 撮影
* 玉虫型偵察器
* Butameron

## 撮影助手
* 伊藤智数

## 字幕・テロップ
*  藻
*  チャンネル君
*  Butameron

## スクリプト開発
* 玉虫型偵察器
* Butameron

## 音響効果
*  がんくま

## 効果音制作
*  がんくま
*  夏野未来
*  Butameron
*  だいすけ （ https://youtu.be/QYmWmrI4ezg )
*  Music is VFR ( CC-BY 4.0 )

## 音楽
*  夏野未来
*  雲丹
*  Taste

## サウンドミキサー
* 　野口あきら

## 挿入歌
「アカネの4ヶ月」 <br />
原曲: 「一週間」（ロシア民謡） <br />
作詞: Butameron <br />
編曲: 夏野未来 <br />
歌: 早稲田大学グリークラブ <br />
* しみたか
* おびの
* きりさん
* ベイちゃん
* 8月もポーチ
* 向日 葵@ピカテュウのお面
* @smithreen
* GTタカオ
* おっくん
* AK2
* SEKAINORYO
* クリプチッド
* なおゆき
* こっしー

## 鉄道設定
* Butameron
* ふぶき

## 技術監修
*  井二かける
*  Butameron

## 脚本チェック
* かわしろ21 
* AyneInchNails 
* yoshina 

## スペシャルサンクス

※掲載は本作品への賛同や支持を示すものではありません。

### 宣伝協力
*  虚構新聞社 http://kyoko-np.net/
*  5号車友の会

### 取材協力・資料提供
*  ひたちなか海浜鉄道株式会社
*  独立行政法人都市再生機構 （ＵＲ都市機構）
*  姫路市 手柄山交流ステーション


### サーバー提供
*  ConoHa byGMO

### 開発環境提供
*  Microsoft BizSpark

### 制作支援
* OPAP-JP 賛助会員
* 様々な形でご支援くださった皆様


## オープニングテーマ
「Try!」 <br />
歌: aosaki <br />
作詞: Butameron <br />
作曲/編曲: Melodic Taste  (http://taste-f.sakura.ne.jp/Melodic/)
MIX: 夏野未来

## オープニングアニメーション
### コンテ・演出
*  Butameron

### 作画
* なめたけ
* 上妻九
* るみあ
* すら
* R子
* 夏野未来


## エンディングテーマ
「Traveler」<br />
WIP(http://wip-music.jp/ )<br />
作詞/作曲: ひょん<br />
編曲: WIP<br />

## エンディングアニメーション

### コンテ
* C.伊東
* 夏野未来

### 演出
* 夏野未来

### キャラクター作画
* るみあ
* 夏野未来

### 背景
* Butameron
* Lingerie
* Tashi
* クニキチ
* 上妻九
* 夏野未来
* 安坂　悠
* 櫛

### 撮影
* クニキチ
* 夏野未来

### 撮影助手
*  Butameron

## 参考文献
*  川﨑哲也『昭和30年代の車輌達（堀越和正氏の世界）』 http://sakikawa.michikusa.jp/index.html (2016/03/26閲覧)
*  日本国有鉄道(1958) 『気動車形式図』
*  日本国有鉄道(1951) 『鋼製客車形式図』


## 監督
Butameron
