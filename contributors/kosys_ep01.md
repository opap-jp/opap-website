---
title: こうしす！第1話 クレジット
layout: page
redirect_from:
    - /wiki/redirect/kosys_ep01_authors
    - /wiki/Redirect/kosys_ep01_authors
    - /wiki/こうしす！/第1話クレジット
---

# こうしす！第1話 クレジット



## 企画・制作

* Open Process Animation Project Japan


## 監督

* Butameron


## 脚本

* Butameron


## シナリオチェック
* Taste
* 玉虫型偵察器
* 匿名希望


## 絵コンテ・演出

* 玉虫型偵察器
* Butameron


## キャラクター原案

* Butameron


## キャラクターデザイン

* 絢嶺るり
* 夏野未来


## 色彩設定

* 夏野未来
* すら
* Butameron


## 制服デザイン

* 麦の人


## 鉄道設定

* Butameron
* ふぶき

## 作画

* 夏野未来
* すら
* なめたけ
* るみあ
* 上妻九
* どりあん
* はるまきごはん
* R子
* Butameron


## 作画監督
禁則事項です♥


## 音響

* 焼田 儚
* 夏野未来
* Butameron


## 音楽

* 焼田 儚


## 3Dモデル作成

* sacorama


## 撮影

* 玉虫型偵察器
* Butameron



## オープニングアニメーション

### 絵コンテ・演出
* Butameron

### 撮影/3DCGI
* Butameron

### 作画
* なめたけ
* 上妻九
* るみあ
* すら
* R子
* 夏野未来



## オープニングテーマ
「Try!」 <br />
歌: aosaki <br />
作詞: Butameron <br />
作曲/編曲: Melodic Taste  (http://taste-f.sakura.ne.jp/Melodic/)



## エンディングアニメーション

### 絵コンテ・演出
* 夏野未来

### 撮影
* 伊藤智数

### 作画
* 夏野未来 
* なめたけ
* 櫛

### 3D背景
* 丘理幻視

## エンディングテーマ
「will」<br />
:WIP (http://wip-music.jp)
作詞/作曲:ひょん<br />
編曲:WIP

## 広報

* 芋
* Butameron

## キャスト

* 祝園アカネ: えつむ
* 山家宏佳: 小豆戒斗
* 垂水結菜: 那々海ゆあ
* 英賀保芽依: 桜瀬尋
* 中舟生CIO: ザンジバル斉藤
* 少佐: 芋
* 少年: トール
* おじいさん・他: 岡松　丈 
* 汎用型移動式パキラ: 鈴郷（さとー）
* 113-3811: 国鉄113系電車 近キトC10編成 他
* 悪いハッカー: Butameron
* コンピューターウィルス: Butameron

## スペシャルサンクス

### 賛助メンバーの皆様
* 玉虫型偵察器　他

### ライセンス関連文書翻訳協力
株式会社 高橋翻訳事務所
http://www.takahashi-office.jp/

### 広告掲載
* 虚構新聞社

## その他
*  Tux G2 Illustrations from the website http://tux.crystalxp.net/  (RC1のみ)


## 著作権について

### RC2（v0.9.1）以降のライセンス 

 Copyright (C) 2014 OPAP-JP contributors.
 See <https://opap.jp/wiki/redirect/kosys_ep01_authors> for full list.  
 (すべての貢献者の一覧は <https://opap.jp/wiki/redirect/kosys_ep01_authors> をご覧ください。)
 
 Licensed under the following license(s):
 (以下の著作物利用許諾により利用が許諾されています:)
 
 * CC-BY 2.1 JP
   <http://creativecommons.org/licenses/by/2.1/jp/>
 
 * CC-BY 4.0
  <http://creativecommons.org/licenses/by/4.0/>
 
 * OPAP Upgrade License 1.0 Japan
  <http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp>
 
 Notes :  (補足事項:)
 * For the avoidance of doubt, full list of contributors is a part of this credit.    
    (誤解を避けるために詳述すると、すべての貢献者の一覧はクレジットの一部です。)

### RC1版のライセンス 
 Copyright (C) 2014 OPAP-JP contributors.
 See <https://opap.jp/wiki/redirect/kosys_ep01_authors> for full list.  
 (すべての貢献者の一覧は <https://opap.jp/wiki/redirect/kosys_ep01_authors> をご覧ください。)
 
 Licensed under the following license(s):
 (以下の著作物利用許諾により利用が許諾されています:)
 
 * CC-BY-NC-SA 4.0
   <http://creativecommons.org/licenses/by-nc-sa/4.0>
 
 Notes :  (補足事項:)
 * For the avoidance of doubt, full list of contributors is a part of this credit.    
    (誤解を避けるために詳述すると、すべての貢献者の一覧はクレジットの一部です。)
