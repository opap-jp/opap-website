---
title: 活動案内
---

# 活動案内

## 作品

当団体では、以下の作品シリーズを制作しています。

* [アニメ「こうしす！」](https://kosys.opap.jp/)

## 会報

* [2019年6月号](https://kosys.gitlab.io/kosys-newsletter-201906/)

## 会計報告

* [会計報告](./financial-reports)
