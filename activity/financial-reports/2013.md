---
title: 第2期（2013年12月期）会計報告
layout: page
redirect_from:
    - /wiki/2013年度決算報告
---

# 第2期（2013年12月期）会計報告

## 決算期
2013年01月01日 〜 2013年12月31日

## 会計責任者
井二かける

## 収支明細（収支計算書）

<table class="wikitable">
<tbody><tr>
<th>科目
</th>
<th>金額
</th></tr>
<tr>
<th colspan="2">収入の部
</th></tr>
<tr>
<th style="text-align: left;">1. 雑収入
</th>
<td>
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>自腹</dd></dl>
</th>
<td style="text-align: right;">7,760
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>雑収入計</dd></dl>
</th>
<td style="text-align: right;">7,760
</td></tr>
<tr>
<th style="text-align: left;">収入計
</th>
<td style="text-align: right;">7,760
</td></tr>
<tr>
<th colspan="2">支出の部
</th></tr>
<tr>
<th style="text-align: left;">1. サーバー等運営費
</th>
<td>
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>さくらのVPS（2GB）@1480 * 2ヶ月</dd></dl>
</th>
<td style="text-align: right;">2,960
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>OPAP-JPドメイン登録費（さくらのドメイン取得）</dd></dl>
</th>
<td style="text-align: right;">3,800
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>さくらのメールボックス</dd></dl>
</th>
<td style="text-align: right;">1,000
</td></tr>
<tr>
<th style="text-align: left;">
<dl><dd>サーバー等運営費計</dd></dl>
</th>
<td>7,760
</td></tr>
<tr>
<th style="text-align: left;">支出計
</th>
<td>7,760
</td></tr>
<tr>
<th colspan="2">収支差額
</th></tr>
<tr>
<th style="text-align: left;">当期収支差額
</th>
<td style="text-align: right;">0
</td></tr></tbody></table>

## 損益計算書
<table class="wikitable">
<tbody><tr>
<th>科目</th>
<th>金額
</th></tr>
<tr>
<td>売上高
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>売上原価
</td>
<td style="text-align: right;">0
</td></tr>
<tr style="background-color:#dfd">
<td>売上総利益
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>販売費及び一般管理費
</td>
<td style="text-align: right;">7,760
</td></tr>
<tr style="background-color:#dfd">
<td>営業損失（△）
</td>
<td style="text-align: right;">△7,760
</td></tr>
<tr>
<td>営業外収益
</td>
<td style="text-align: right;">50,500
</td></tr>
<tr>
<td>営業外費用
</td>
<td style="text-align: right;">0
</td></tr>
<tr style="background-color:#dfd">
<td>経常利益
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>特別利益
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>特別損失
</td>
<td style="text-align: right;">0
</td></tr>
<tr style="background-color:#dfd">
<td>税引前当期純利益
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>法人税等
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>法人税等調整額
</td>
<td style="text-align: right;">0
</td></tr>
<tr style="background-color:#dfd">
<td>当期純利益
</td>
<td style="text-align: right;">0
</td></tr></tbody></table>


## 貸借対照表
<table class="wikitable">
<tbody><tr>
<th>科目</th>
<th>金額</th>
<th>科目</th>
<th>金額
</th></tr>
<tr>
<th colspan="2">資産の部
</th>
<th colspan="2">負債の部
</th></tr>
<tr>
<td>流動資産
</td>
<td style="text-align: right;">50,500
</td>
<td>流動負債
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>固定資産
</td>
<td style="text-align: right;">0
</td>
<td>固定負債
</td>
<td style="text-align: right;">0
</td></tr>
<tr>
<td>
</td>
<td>
</td>
<td style="background-color:#dfd">負債合計
</td>
<td style="text-align: right; background-color:#dfd;">0
</td></tr>
<tr>
<td>
</td>
<td>
</td>
<th colspan="2">純資産の部
</th></tr>
<tr>
<td>
</td>
<td>
</td>
<td>余剰金
</td>
<td style="text-align: right;">50,500
</td></tr>
<tr>
<td>
</td>
<td>
</td>
<td style="background-color:#dfd">純資産合計
</td>
<td style="text-align: right; background-color:#dfd;">0
</td></tr>
<tr style="background-color:#dfd">
<td>資産合計
</td>
<td style="text-align: right;">50,500
</td>
<td>純資産負債合計
</td>
<td style="text-align: right;">50,500
</td></tr></tbody></table>

## 注記
