---
title: 第8期（2019年12月期）会計報告
layout: page
---


# 第8期（2019年12月期）会計報告
## 決算期
2019年01月01日 〜 2019年12月31日

## 会計責任者
井二かける

## 活動計算書（損益計算書）
<div class="unit">
    単位：円
</div>
<div><table class="financialSheet"><colgroup><col class="col-accountName"> <col class="col-balance"> <col class="col-balance"> <col class="col-balance"></colgroup> <thead><tr><th>科目</th> <th colspan="3">金額</th></tr></thead> <tbody><tr class="item-depth-0 item-group-header"><td class="accountName">I 経常収益</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header"><td class="accountName">1.受取会費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">正会員受取会費</td> <td class="balance balance-1">5,000</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">賛助会員受取会費</td> <td class="balance balance-1">62,467</td> <td class="balance balance-2">67,467</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header item-break-1"><td class="accountName">2.受取寄附金</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">受取寄附金</td> <td class="balance balance-1">624,960</td> <td class="balance balance-2">624,960</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header item-break-1"><td class="accountName">3.その他収益</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">雑収益</td> <td class="balance balance-1">42,809</td> <td class="balance balance-2">42,809</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-sum item-break-1 item-break-2"><td class="accountName">経常収益計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">735,236</td></tr><tr class="item-depth-0 item-group-header"><td class="accountName">II 経常費用</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header"><td class="accountName">1.事業費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-group-header"><td class="accountName">（1）人件費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3 item-sum item-break-1"><td class="accountName">人件費計</td> <td class="balance balance-1">0</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-group-header item-break-1"><td class="accountName">（2）その他経費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">業務委託費</td> <td class="balance balance-1">445,263</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">通信運搬費</td> <td class="balance balance-1">21,606</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">消耗品費</td> <td class="balance balance-1">4,683</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">支払手数料</td> <td class="balance balance-1">57,520</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">広告宣伝費</td> <td class="balance balance-1">9,441</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3 item-sum item-break-1"><td class="accountName">その他経費計</td> <td class="balance balance-1">538,513</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">事業費計</td> <td class="balance balance-1"></td> <td class="balance balance-2">538,513</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header"><td class="accountName">2.管理費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-group-header"><td class="accountName">（1）人件費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3 item-sum item-break-1"><td class="accountName">人件費計</td> <td class="balance balance-1">0</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-group-header item-break-1"><td class="accountName">（2）その他経費</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">印刷製本費</td> <td class="balance balance-1">37,072</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">通信運搬費</td> <td class="balance balance-1">124,418</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">消耗品費</td> <td class="balance balance-1">1,183</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3"><td class="accountName">支払手数料</td> <td class="balance balance-1">41,203</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-3 item-sum item-break-1"><td class="accountName">その他経費計</td> <td class="balance balance-1">203,876</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">管理費計</td> <td class="balance balance-1"></td> <td class="balance balance-2">203,876</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-sum item-break-2"><td class="accountName">経常費用計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">742,389</td></tr><tr class="item-depth-2 item-sum item-group-header item-break-3"><td class="accountName">当期経常増減額</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">△ 7,153</td></tr><tr class="item-depth-0 item-group-header"><td class="accountName">III経常外収益</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-group-header"><td class="accountName">過年度損益修正益</td> <td class="balance balance-1">10,274</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">経常外収益計</td> <td class="balance balance-1">10,274</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-0 item-break-1 item-break-3"><td class="accountName">&nbsp;当期正味財産増減額</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">3,121</td></tr><tr class="item-depth-2 item-sum"><td class="accountName">前期繰越正味財産額</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">△ 51,893</td></tr><tr class="item-depth-2 item-sum item-break-3"><td class="accountName">次期繰越正味財産額</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">△ 48,772</td></tr></tbody></table></div>


## 貸借対照表
<div class="unit">
    単位：円
</div>
<div><table class="financialSheet"><colgroup><col class="col-accountName"> <col class="col-balance"> <col class="col-balance"> <col class="col-balance"></colgroup> <thead><tr><th>科目</th> <th colspan="3">金額</th></tr></thead> <tbody><tr class="item-depth-0 item-group-header"><td class="accountName">Ⅰ 資産の部</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header"><td class="accountName">1.流動資産</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">現金預金</td> <td class="balance balance-1">16,261</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">流動資産合計</td> <td class="balance balance-1"></td> <td class="balance balance-2">16,261</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-group-header"><td class="accountName">2.固定資産</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">固定資産合計</td> <td class="balance balance-1"></td> <td class="balance balance-2">0</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-sum item-break-2"><td class="accountName">資産合計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">16,261</td></tr><tr class="item-depth-0 item-group-header item-break-3"><td class="accountName">Ⅱ 負債の部</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-1"><td class="accountName">1.流動負債</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">役員借入金</td> <td class="balance balance-1">65,033</td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-group-header item-break-1"><td class="accountName">流動負債合計</td> <td class="balance balance-1"></td> <td class="balance balance-2">65,033</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1"><td class="accountName">2.固定負債</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2 item-sum item-break-1"><td class="accountName">固定負債合計</td> <td class="balance balance-1"></td> <td class="balance balance-2">0</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-sum item-break-2"><td class="accountName">負債合計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">65,033</td></tr><tr class="item-depth-0 item-group-header"><td class="accountName">Ⅲ 正味財産の部</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">前期繰越正味財産</td> <td class="balance balance-1"></td> <td class="balance balance-2">△ 51,893</td> <td class="balance balance-3"></td></tr><tr class="item-depth-2"><td class="accountName">当期正味財産増減額</td> <td class="balance balance-1"></td> <td class="balance balance-2">3,121</td> <td class="balance balance-3"></td></tr><tr class="item-depth-1 item-sum item-break-2"><td class="accountName">正味財産合計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">△ 48,772</td></tr><tr class="item-depth-1 item-sum item-break-3"><td class="accountName">負債及び正味財産合計</td> <td class="balance balance-1"></td> <td class="balance balance-2"></td> <td class="balance balance-3">16,261</td></tr></tbody></table></div>



## 財務諸表の注記
### 重要な会計方針

財務諸表の作成は、NPO法人会計基準（2010年7月20日　2017年12月12日最終改正　NPO法人会計基準 協議会）によっています。ただし、財産目録の作成は省略しております。


### 施設の提供等の物的サービスの受入の内訳
本年度は計上していません。

### 活動の原価の算定にあたって必要なボランティアによる役務の提供の内訳
本年度は計上していません。

### 借入金の増減内訳
<div class="unit">
単位：円
</div>
<table class="financialSheet"><tr><th>科目</th> <th>当期残高</th> <th>当期借入</th> <th>当期返済</th> <th>期末残高</th></tr> <tr><td>役員借入金</td> <td>54,000</td> <td>76,753</td> <td>65,720</td> <td>65,033</td></tr> <tr><td>計</td> <td>54,000</td> <td>76,753</td> <td>65,720</td> <td>65,033</td></tr></table>

### 役員及びその近親者との取引の内容

役員及びその近親者との取引は以下の通りです。

#### 活動計算書
<div class="unit">
単位：円
</div>
<table class="financialSheet"><tr><th>科目</th> <th>計算書類に計上された金額</th> <th>内、役員との取引</th> <th>内、近親者及び支配法人等との取引</th></tr> <tr><td>正会員受取会費</td> <td>5,000</td> <td>5,000</td> <td>0</td></tr> <tr><td>雑収益</td> <td>39,954</td> <td>39,954</td> <td>0</td></tr> <tr><td>計</td> <td>44,954</td> <td>44,954</td> <td>0</td></tr></table>

#### 貸借対照表
<div class="unit">
単位：円
</div>
<table class="financialSheet"><tr><th>科目</th> <th>計算書類に計上された金額</th> <th>内、役員との取引</th> <th>内、近親者及び支配法人等との取引</th></tr> <tr><td>役員借入金</td> <td>185,007</td> <td>185,007</td> <td>0</td></tr> <tr><td>計</td> <td>185,007</td> <td>185,007</td> <td>0</td></tr></table>

<style>
.hidden { visibility: hidden; }
.unit{
    text-align:right;
    width: 800px;
}
.financialSheet{
    border-collapse: collapse;
    width: 800px;
    table-layout:fixed;
    border:1px solid #000;
    display:table;
    margin:0
}
.financialSheet *{
    background:none
}
.financialSheet tr{
    border:none
}
.financialSheet td,.financialSheet th{
    border:1px solid #000
}
.financialSheet td{
    border-top:none;
    border-bottom:none;
    padding:5px
}
.financialSheet .item-depth-1 .accountName{
    padding-left:2em
}
.financialSheet .item-depth-2 .accountName{
    padding-left:4em
}
.financialSheet .item-depth-3 .accountName{
    padding-left:6em
}
.financialSheet .balance{
    text-align:right
}
.financialSheet .item-group-header .accountName,.financialSheet .item-sum .accountName,.financialSheet .item-sum .balance{
    font-weight:700
}
.financialSheet .item-break-1 .balance-1,.financialSheet .item-break-2 .balance-2,.financialSheet .item-break-3 .balance-3{
    border-top:1px solid #000
}
.col-accountName{
    width:auto
}
.col-balance{
    width:8em
}
</style>