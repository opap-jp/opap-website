---
layout: page
permalink: /404.html
---

# ページが見つかりませんでした

現在リニューアル作業中につき、一部のページが移行されていません。  
ご不便をおかけし大変申し訳ございません。

<div id="wiki-guide">
お探しのページは、旧wikiに存在するかもしれません。<br />
<a href="https://wiki.opap.jp" id="wiki-guide-link">旧wikiで閲覧を試みる場合はこちらをクリック</a>
</div>
<script>
    (function(){
        var link = document.getElementById("wiki-guide-link");
        link.href = "https://wiki.opap.jp" + location.pathname;
    })();
</script>

[トップページへ戻る](/)